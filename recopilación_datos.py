import csv
import requests
from datetime import datetime
import time

# tus credenciales de la API de Twitch
CLIENT_ID = '5s6wtyqmd92tbz1bs1872d6mpqpz1x'
CLIENT_SECRET = '1hoi4t3p708jh759xmaufmp8w5qvlp'
OAUTH_URL = 'https://id.twitch.tv/oauth2/token'

# abrir el archivo en modo de escritura para agregar la línea de cabecera
with open('twitch_data.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['Game Name', 'Current Viewers', 'Total Viewers', 'Timestamp'])

while True:
    # obtener el token de acceso
    payload = {
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'client_credentials'
    }
    response = requests.post(OAUTH_URL, params=payload)
    response.raise_for_status()
    access_token = response.json()['access_token']

    # preparar los datos para el gráfico
    data = {}

    # llamar a la API de Twitch
    HEADERS = {
        'Client-ID': CLIENT_ID,
        'Authorization': f'Bearer {access_token}'
    }

    pagination_cursor = ''
    for _ in range(2):  # hacer 2 solicitudes para obtener el top 200
        params = {'first': 100}  # solicitar el máximo de elementos
        if pagination_cursor:
            params['after'] = pagination_cursor  # añadir cursor para paginación si existe

        response = requests.get('https://api.twitch.tv/helix/streams', headers=HEADERS, params=params)
        response.raise_for_status()

        # obtener datos de transmisiones y añadirlos a la lista
        json_response = response.json()
        for stream in json_response['data']:
            game_name = stream['game_name']
            current_viewers = stream['viewer_count']
            if game_name not in data:
                data[game_name] = 0
            data[game_name] += current_viewers

            # guardar los datos en un archivo CSV
            with open('twitch_data.csv', 'a', newline='') as f:
                writer = csv.writer(f)
                writer.writerow([game_name, current_viewers, data[game_name], time.ctime()])

        # obtener el cursor de paginación para la próxima solicitud
        pagination_cursor = json_response['pagination'].get('cursor', '')

